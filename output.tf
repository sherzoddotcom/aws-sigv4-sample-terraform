output "apigw-vpce-id" {
    value = module.apigw-vpce.endpoint_id
}

output "private-subnet-ids" {
    value = data.aws_subnets.selected.ids
}

output "apigw-vpce-sg-id" {
    value = module.apigw-vpce.security_group_id
}

output "bastion-instance-id" {
    value = module.ec2-bastion.instance-id
}