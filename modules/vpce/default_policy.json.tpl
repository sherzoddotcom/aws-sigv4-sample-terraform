{
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "AllowAll",
        "Effect" : "Allow",
        "Principal" : {
          "AWS" : "*"
        },
        "Action" : [
          "${service_name}:*"
        ],
        "Resource" : "*",
        "Condition": {
          "StringEquals": {
            "aws:PrincipalOrgID": "${org_id}"
          }
        }
      }
    ]
}