data "aws_region" "current" {}
data "aws_organizations_organization" "this" {}

data "aws_subnet" "one" {
  id = var.subnet_ids[0]
}

data "aws_vpc" "this" {
    id = data.aws_subnet.one.vpc_id
}

resource "aws_vpc_endpoint" "this" {
  vpc_id            = data.aws_vpc.this.id
  service_name      = "com.amazonaws.${data.aws_region.current.name}.${var.service_name}"
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    aws_security_group.this.id
  ]
  subnet_ids = var.subnet_ids
  private_dns_enabled = true
}

resource "aws_vpc_endpoint_policy" "this" {
  vpc_endpoint_id = aws_vpc_endpoint.this.id
  policy = templatefile("${path.module}/default_policy.json.tpl", {
    service_name = var.service_name
    org_id = data.aws_organizations_organization.this.id
  })
}

resource "aws_security_group" "this" {
    name = "${var.service_name}-vpce-sg"
    description = "Allow TLS inbound"
    vpc_id = data.aws_vpc.this.id

    ingress {
        description = "TLS from VPC"
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = [data.aws_vpc.this.cidr_block]
    }

    egress {
        description = "Allow all out"
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }
}