output "endpoint_id" {
    value = aws_vpc_endpoint.this.id
}

output "security_group_id" {
    value = aws_security_group.this.id
}