variable "service_name" {
    type = string
}

variable "subnet_ids" {
    type = list(string)
}
variable "app_name" {
    type = string
    default = "acme"
}
