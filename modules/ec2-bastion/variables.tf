variable "subnet_id" {
    type = string
}

variable "app_name" {
    type = string
    default = "acme"
}

variable "type" {
    type = string
    default = "t3a.micro"
}

variable "ami" {
    type = string
    default = "ami-090fa75af13c156b4"
}