
data "aws_subnet" "this" {
  id = var.subnet_id
}

resource "aws_security_group" "this" {
    name = "${var.app_name}-ec2-bastion-sg"
    description = "Allow TLS inbound"
    vpc_id = data.aws_subnet.this.vpc_id

    egress {
        description = "Allow all out"
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        ipv6_cidr_blocks = ["::/0"]
    }
}

resource "aws_iam_role" "this" {
  name = "${var.app_name}-ec2-bastion-role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  managed_policy_arns = toset([
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  ])
}

resource "aws_iam_instance_profile" "this" {
  name = "${var.app_name}-ec2-bastion-instance-profile"
  role = aws_iam_role.this.name
}

resource "aws_instance" "this" {
  ami = var.ami
  instance_type = var.type
  iam_instance_profile = aws_iam_instance_profile.this.name
  subnet_id = var.subnet_id
  vpc_security_group_ids = [aws_security_group.this.id]
}