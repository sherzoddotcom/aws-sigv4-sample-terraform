data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = ["foobar-vpc"]
  }
}

data "aws_subnets" "selected" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.selected.id]
  }

  filter {
    name = "tag:Name"
    values = ["foobar-vpc-private-us-east-1a", "foobar-vpc-private-us-east-1b"]
  }
}

module "apigw-vpce" {
  source       = "./modules/vpce"
  service_name = "execute-api"
  subnet_ids   = data.aws_subnets.selected.ids
}

module "ec2-bastion" {
  source = "./modules/ec2-bastion"
  subnet_id   = data.aws_subnets.selected.ids[0]
}